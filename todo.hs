{-# OPTIONS -Wall #-}
import System.IO
import System.Environment
import System.Directory
import Control.Exception
import Data.List

main = do
    (command:argList) <- getArgs
    dispatch command argList

dispatch :: String -> [String] -> IO ()
dispatch "add" = add
dispatch "view" = view
dispatch "remove" = remove
dispatch _ = error "no match method"

add :: [String] -> IO()
add [fileName, todoItem] = appendFile fileName (todoItem ++ "\n")

view :: [String] -> IO ()
view [fileName] = do
    contents <- readFile fileName
    let todoTasks = lines contents
        numberedTasks = zipWith (\n line -> show n ++ " " ++ line) [0..] todoTasks
    putStrLn $ unlines numberedTasks

remove :: [String] -> IO()
remove [fileName, numberString] = do
    contents <- readFile fileName
    let number = read numberString
        oldTodoItems = zipWith (\n line -> show n ++ " " ++ line) [0..] $ lines contents
        newTodoItems = unlines $ delete (oldTodoItems !! number) oldTodoItems
    bracketOnError  {- ERROR -}
        (openTempFile "." "temp")
        (\ (tempName, tempHandle) -> do
            hClose tempHandle
            removeFile tempName)
        (\ (tempName, tempHandle) -> do
            hPutStr tempHandle newTodoItems
            hClose tempHandle
            removeFile fileName
            renameFile tempName fileName)
